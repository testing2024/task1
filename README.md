# Приложение Spring Boot React Full Stack Application

- Докеризовать проект: бэк и фронт запускаюся в докере и видят друг друга
- Установить nginx перед фронтом, убрать внешний доступ до контейнеров, оставив только nginx
- Docker-compose файд поднимающий весь инстанс: backend, frontend, db и nginx
- Добавим переменные окружения env_file и environment